import os, json
import time
import requests

from flask import Flask, session, flash, render_template, redirect, url_for, template_rendered
from flask_session import Session
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from datetime import date
from datetime import time

from werkzeug.security import check_password_hash, generate_password_hash

app = Flask(__name__)

res = requests.get("https://www.goodreads.com/book/review_counts.json", params={"YqPfMW5t9MbuGlsj57WJg": "YqPfMW5t9MbuGlsj57WJg", "isbns": "9781632168146"})

# Check for environment variable
if not os.getenv("DATABASE_URL"):
    raise RuntimeError("DATABASE_URL")

# Configure session to use filesystem
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
app.config["DATABASE_URL"] = "postgres://noztkjunljqlmc:d589665a5e8668263ccfa7eac5986da1c9c0047fd9db42ea54685cf27a73f8d5@ec2-52-44-166-58.compute-1.amazonaws.com:5432/d4t014mqa9mcp6"
Session(app)

# Set up database
engine = create_engine(os.getenv("DATABASE_URL"))
db = scoped_session(sessionmaker(bind=engine))

goodreads_key = 'YqPfMW5t9MbuGlsj57WJg'
project1 = 'global'

res = requests.get("https://www.goodreads.com/book/review_counts.json", params={"key": gr_key, "isbns": "9781632168146"})

# Index Main Route     
@app.route("/")
def index():
    "Welcome to Rotten Pages"
    return render_template("index.html",info_msg="Login to review books")

# Login User
@app.route("/login", METHODS=["GET", "POST"])
def login():
    session.clear

    if request.method == "POST":
        if not request.form.get ("username"):
            return render_template("login.html",message="Please enter your username.")
        elif not request.form.get("password"):
            return render_template("login.html",message="Please enter your password.")
        else: 
            username = request.form.get ("username")
            rows = db.execute("SELECT * FROM users WHERE username = :username",
                            {"username": username})
        
        result = rows.fetchone()
       
    if result == None or not check_password_hash(result[2], request.form.get("password")):
        return render_template("error.html", message="invalid username and/or password")

        # Remember which user has logged in
        session["user_id"] = result[0]
        session["user_name"] = result[1]

        return redirect("/")
    else:
        return render_template("login.html")

@app.route("/register", methods=["GET", "POST"])
def register():
    session.clear()
    if request.method == "POST":
        if not request.form.get ("username"):
            return render_template("error.html", message="invalid username or username already exists. Choose new username.")




        

